from django.apps import AppConfig


class VpnApplicationConfig(AppConfig):
    name = 'vpn_application'

    def ready(self):
        super().ready()
        self.register_signals()

    def register_signals(self):
        from django_auth_ldap.backend import populate_user
        from vpn_application.signals import ldap_user_created
        populate_user.connect(ldap_user_created, dispatch_uid="my_unique_identifier")


