from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

from vpn_application.models import KeyPairRequest


def ldap_user_created(sender, **kwargs):
    user = kwargs['user']
    content_type = ContentType.objects.get_for_model(KeyPairRequest)
    add_key_pair_request = Permission.objects.get(codename='add_keypairrequest', content_type=content_type)
    change_key_pair_request = Permission.objects.get(codename='change_keypairrequest', content_type=content_type)
    user.user_permissions.add(add_key_pair_request, change_key_pair_request)
