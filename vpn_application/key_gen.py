from OpenSSL import crypto

from vpnmanager.private_settings import CERT_PATH, KEY_PATH, KEY_PASSPHRASE

with open(CERT_PATH, "r") as my_cert_file:
    my_cert_text = my_cert_file.read()
    CERT = crypto.load_certificate(crypto.FILETYPE_PEM, my_cert_text)

with open(KEY_PATH, "r") as my_cert_file:
    my_cert_text = my_cert_file.read()
    KEY = crypto.load_privatekey(crypto.FILETYPE_PEM, my_cert_text, KEY_PASSPHRASE.encode('utf-8'))


def generate_private_key(key_alg=crypto.TYPE_RSA, key_size=2048):
    private_key = crypto.PKey()
    private_key.generate_key(type=key_alg, bits=key_size)
    return private_key


def sign_csr_with_ca(certificate_sign_request, serial):
    csr = crypto.load_certificate_request(
        crypto.FILETYPE_PEM,
        certificate_sign_request
    )
    certificate = create_certificate(csr, CERT, KEY, serial, 0, 60 * 24 * 365 * 10)
    dumped_certificate = crypto.dump_certificate(crypto.FILETYPE_PEM, certificate)
    return dumped_certificate.decode()


def generate_csr(private_key, digest, **kwargs):
    csr = crypto.X509Req()
    subj = csr.get_subject()
    subj.C = kwargs['country']
    subj.ST = kwargs['state']
    subj.L = kwargs['location']
    subj.O = kwargs['organization']
    subj.OU = kwargs['organization_unit']
    subj.CN = kwargs['common_name']
    csr.set_pubkey(private_key)
    csr.sign(private_key, digest)
    return csr


def generate_key_request_pair(key_kwargs):
    encoded_password = get_encoded_password(key_kwargs)
    private_key = generate_private_key()
    return (
        crypto.dump_privatekey(type=crypto.FILETYPE_PEM, pkey=private_key, passphrase=encoded_password).decode(),
        crypto.dump_certificate_request(crypto.FILETYPE_PEM, generate_csr(private_key, 'sha256', **key_kwargs)).decode()
    )


def get_encoded_password(key_kwargs):
    password = key_kwargs.pop('passphrase', None)
    if password is not None:
        encoded_password = password.encode("utf-8")
    else:
        encoded_password = ''.encode("utf-8")
    return encoded_password


def create_certificate(req, issuerCert, issuerKey, serial, notBefore, notAfter, digest="sha256"):
    """
    Generate a certificate given a certificate request.
    Arguments: req        - Certificate request to use
               issuerCert - The certificate of the issuer
               issuerKey  - The private key of the issuer
               serial     - Serial number for the certificate
               notBefore  - Timestamp (relative to now) when the certificate
                            starts being valid
               notAfter   - Timestamp (relative to now) when the certificate
                            stops being valid
               digest     - Digest method to use for signing, default is sha256
    Returns:   The signed certificate in an X509 object
    """
    cert = crypto.X509()
    cert.set_serial_number(serial)
    cert.gmtime_adj_notBefore(notBefore)
    cert.gmtime_adj_notAfter(notAfter)
    cert.set_issuer(issuerCert.get_subject())
    cert.set_subject(req.get_subject())
    cert.set_pubkey(req.get_pubkey())
    cert.sign(issuerKey, digest)
    return cert
