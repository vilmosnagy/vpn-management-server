from OpenSSL import crypto
from django.contrib.auth.models import User
from django.db import models


class KeyPairRequest(models.Model):
    name = models.CharField(max_length=255, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    requested_at = models.DateTimeField(auto_now_add=True)
    is_signed = models.BooleanField(default=False)
    is_revoked = models.BooleanField(default=False)
    key_size = models.IntegerField()
    expiration_date = models.DateTimeField()
    certificate_sign_request = models.TextField(null=True, max_length=65535)
    signed_key = models.TextField(null=True, max_length=65535, blank=True)

    def __str__(self):
        csr = 'None'
        if (self.certificate_sign_request is not None):
            try:
                csr = crypto.load_certificate_request(
                    crypto.FILETYPE_PEM,
                    self.certificate_sign_request
                )
                csr = str(dict(csr.get_subject().get_components()))
            except Exception:
                csr = "Can't load CSR"
        return self.user.username + ' - ' + self.name + '; CSR: ' + csr

    class Meta:
        unique_together = (("name", "user"),)
