from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms.utils import ErrorList


class KeyPairRequestCreation(forms.ModelForm):
    user = forms.ChoiceField(disabled=True, widget=forms.HiddenInput)
    name = forms.CharField(
        required=True,
        label='Elnevezés',
        help_text='Egy név, amivel később listázhatod a saját VPN kulcsaidat. Pl.: céges-laptop'
    )
    passphrase = forms.CharField(
        empty_value=None,
        widget=forms.PasswordInput,
        required=False,
        label='Passphrase',
        help_text='Amennyiben azt szeretnéd, hogy mi generáljunk neked privát kulcsot, s szeretnél hozzá jelszót beállítani, add meg.'
    )
    certificate_sign_request = forms.CharField(
        empty_value=None,
        required=False,
        widget=forms.Textarea,
        label='Certificate Sign Request',
        help_text='Ha van már generált privát kulcsod (vagy nem szeretnéd, ha mi generálnánk), akkor tölts fel egy CSR-t.'
    )

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)
        self.__name__ = 'KeyPairRequestCreation'
        self.initial['user'] = self.request.user
        self.fields['user'] = forms.ChoiceField(choices=[(self.request.user.id, self.request.user.username)])

    def clean_user(self):
        return self.request.user

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['passphrase'] is not None and cleaned_data['certificate_sign_request'] is not None:
            raise ValidationError("Egyszerre csak a Passphrase vagy a CSR mezőt töltsd ki!")
        return cleaned_data


class KeyPairRequestEdit(forms.ModelForm):

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None, initial=None, error_class=ErrorList,
                 label_suffix=None, empty_permitted=False, instance=None, use_required_attribute=None):
        super().__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance,
                         use_required_attribute)

        if not self.request.user.is_superuser:
            self.fields['certificate_sign_request'].disabled=True
            self.fields['signed_key'].disabled=True
        self.fields['user'].disabled = True
        self.fields['user'].widget = forms.HiddenInput()
