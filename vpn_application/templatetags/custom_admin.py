from django.contrib.admin.templatetags import admin_modify
from django import template

register = template.Library()

@register.inclusion_tag('admin/submit_line.html', takes_context=True)
def custom_submit_row(context):
    ret_cxt = admin_modify.submit_row(context)
    ret_cxt['show_save_and_continue'] = ret_cxt['show_save_and_continue'] and context.get('show_save_and_continue', True)
    ret_cxt['show_save_and_add_another'] = ret_cxt['show_save_and_add_another'] and context.get('show_save_and_add_another', True)
    return ret_cxt