from django.contrib import admin
from django.core.mail import send_mail
from django.template.response import TemplateResponse

from vpn_application import key_gen
from vpn_application.forms import KeyPairRequestCreation, KeyPairRequestEdit
from vpn_application.key_gen import sign_csr_with_ca
from vpn_application.models import KeyPairRequest
from vpnmanager import private_settings
from vpnmanager.private_settings import build_user_success_email_body, EMAIL_FROM, build_user_success_email_subject, \
    EMAIL_ENABLED


def send_success_mail(entity):
    send_mail(
        build_user_success_email_subject(entity),
        build_user_success_email_body(entity),
        EMAIL_FROM,
        [entity.user.email],
        fail_silently=False,
    )


def sign_certificate_requests(modeladmin, request, queryset):
    for entity in queryset:
        dumped_certificate = sign_csr_with_ca(entity.certificate_sign_request, entity.id)
        entity.signed_key = dumped_certificate
        entity.is_signed = True
        entity.save()
        if EMAIL_ENABLED:
            send_success_mail(entity)


sign_certificate_requests.short_description = 'Írd alá a kiválasztott CSR-eket.'


class CSRNotNullFilter(admin.SimpleListFilter):
    title = 'CSRrel rendelkezik'
    parameter_name = 'certificate_sign_request'

    def lookups(self, request, model_admin):
        return (
            ('not_null', 'Rendelkezik'),
            ('null', 'Nem rendelkezik'),
        )

    def queryset(self, request, queryset):
        filter_string = self.parameter_name + '__isnull'
        if self.value() == 'not_null':
            is_null_false = {
                filter_string: False
            }
            return queryset.filter(**is_null_false)

        if self.value() == 'null':
            is_null_true = {
                filter_string: True
            }
            return queryset.filter(**is_null_true)


@admin.register(KeyPairRequest)
class KeyPairRequestAdmin(admin.ModelAdmin):
    add_form = KeyPairRequestCreation
    add_form_template = "vpn_application/key_pair_request/add_form.html"
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('user', 'name', 'passphrase', 'certificate_sign_request'),
        }),
    )
    list_display = ('user', 'name', 'requested_at', 'is_signed', 'is_revoked')
    form = KeyPairRequestEdit
    search_fields = ('user__username', 'name',)
    list_filter = ('requested_at', 'is_signed', 'is_revoked', CSRNotNullFilter)
    actions = [sign_certificate_requests]

    def __init__(self, model, admin_site):
        super().__init__(model, admin_site)
        self.saved_form = None

    def get_form(self, request, obj=None, **kwargs):
        defaults = {}
        if obj is None:
            defaults['form'] = self.add_form
        defaults.update(kwargs)
        if not request.user.is_superuser:
            if self.exclude is None:
                self.exclude = tuple()
            self.exclude += ('is_signed', 'is_revoked')

        form = super(KeyPairRequestAdmin, self).get_form(request, obj, **defaults)
        form.request = request
        return form

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(user=request.user)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            if 'sign_certificate_requests' in actions:
                del actions['sign_certificate_requests']
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        return super(KeyPairRequestAdmin, self).get_fieldsets(request, obj)

    def add_view(self, request, form_url='', extra_context=None):
        if extra_context is None:
            extra_context = {}
        extra_context['show_save_and_add_another'] = False
        extra_context['show_save_and_continue'] = False
        return super().add_view(request, form_url, extra_context)

    def save_form(self, request, form, change):
        self.saved_form = form
        return super().save_form(request, form, change)

    def response_add(self, request, obj, post_url_continue=None):
        dumped_private_key = None
        dumped_csr = self.saved_form.cleaned_data['certificate_sign_request']
        if dumped_csr is None:
            key_kwargs = private_settings.map_user_to_generate_key_kwargs(request.user)
            if self.saved_form.cleaned_data['passphrase'] is not None:
                key_kwargs['passphrase'] = self.saved_form.cleaned_data['passphrase']
            (dumped_private_key, dumped_csr) = key_gen.generate_key_request_pair(key_kwargs)
        obj.certificate_sign_request = dumped_csr
        obj.user = request.user
        obj.save()
        context = {
            'private_key': dumped_private_key
        }
        return TemplateResponse(request, 'vpn_application/key_pair_request/created.html', context)
