# Generated by Django 2.0 on 2018-03-05 17:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vpn_application', '0002_auto_20180221_2116'),
    ]

    operations = [
        migrations.RenameField(
            model_name='keypairrequest',
            old_name='is_valid',
            new_name='is_signed',
        ),
        migrations.AddField(
            model_name='keypairrequest',
            name='is_revoked',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='keypairrequest',
            name='key_size',
            field=models.IntegerField(default=2048),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='keypairrequest',
            name='name',
            field=models.CharField(max_length=255, null=True),
        ),
    ]
