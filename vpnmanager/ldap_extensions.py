from django_auth_ldap.config import LDAPGroupQuery


class DummyLdapGroupQuery(LDAPGroupQuery):
    def __init__(self, value, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value

    def resolve(self, ldap_user, groups=None):
        return self.value