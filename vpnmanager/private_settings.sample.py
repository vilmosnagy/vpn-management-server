import ldap
from django_auth_ldap.config import LDAPSearch, LDAPGroupQuery, GroupOfUniqueNamesType

AUTH_LDAP_SERVER_URI = "ldap://ldap.company.com"
AUTH_LDAP_BIND_DN = ""
AUTH_LDAP_BIND_PASSWORD = ""
AUTH_LDAP_USER_SEARCH = LDAPSearch("ou=users,dc=company,dc=com", ldap.SCOPE_SUBTREE, "(uid=%(user)s)")

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_active": LDAPGroupQuery("ou=users,ou=groups,dc=company,dc=com"),
    "is_staff": LDAPGroupQuery("ou=users,ou=groups,dc=company,dc=com"),
    "is_superuser": LDAPGroupQuery("ou=admin,ou=groups,dc=company,dc=com"),
}

AUTH_LDAP_GROUP_SEARCH = LDAPSearch("ou=groups,dc=company,dc=com", ldap.SCOPE_SUBTREE, "(objectClass=groupOfUniqueNames)")
AUTH_LDAP_GROUP_TYPE = GroupOfUniqueNamesType()


def map_user_to_generate_key_kwargs(django_user):
    """
    Should return a dict which contains the defines keys. This'll be used to generate the private key for the user
    :param django_user:
    :return:
    """
    return {
        'country': "CO",
        'state': "STATE",
        'location': "CITY",
        'organization': "Company Ltd.",
        'organization_unit': "Test Unit",
        'common_name': django_user.ldap_user.attrs['cn'][0],
    }


CERT_PATH = "<define me>"
KEY_PATH = "<define me>"
KEY_PASSPHRASE = "<define me>"

EMAIL_ENABLED = True
EMAIL_HOST = 'smtp.server.com'
EMAIL_PORT = 25

EMAIL_FROM = 'noreply@company.com'


# TODO use Jinja instead
def build_user_success_email_subject(csr):
    return 'Subject'


# TODO use Jinja instead
def build_user_success_email_body(csr):
    return 'Body'