asn1crypto==0.24.0
cffi==1.11.2
cryptography==2.1.4
Django==2.0
django-auth-ldap==1.2.16
idna==2.6
pycparser==2.18
pyldap==2.4.37
pyOpenSSL==17.5.0
pytz==2017.3
six==1.11.0
